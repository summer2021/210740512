import React from 'react'
import { useTheme, Select } from '@geist-ui/react'
import ButtonRound from "./Button/Round";
import Moon from "@geist-ui/react-icons/moon";
import Sun from "@geist-ui/react-icons/sun";

const Home = ({ onThemeChange }) => {
  const theme = useTheme()
  const changeHandler = val => {
    onThemeChange && onThemeChange(val)
  }
  return (
    <div>
      <Select size="small" value={theme.type} onChange={changeHandler}>
        <Select.Option label>System preset</Select.Option>
        <Select.Option value="light">Light</Select.Option>
        <Select.Option value="dark">Dark</Select.Option>
      </Select>
      {/* <ButtonRound 
          aria-label="Theme"
          icon={theme.type === "dark" ? <Moon /> : <Sun />}
          onClick={changeHandler} />  */}
    </div>
  )
}

export default Home

