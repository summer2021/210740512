import React, { Component, useState } from 'react';
import ReactDOM from 'react-dom';
import { GeistProvider, CssBaseline, Page, Text } from '@geist-ui/react'
import Home from './home'
import CssDemo from './components/cssDemo';
import data from '../data/index'
import StackedLineChart from './components/echarts/line/Stacked Line Chart'
import Doughnut from './components/echarts/pie/Doughnut Chart'
import { Table } from 'antd';
import 'antd/dist/antd.css'; 
import App1 from './test/App';


class Title extends Component {
    render () {
      return (
        <h1>Open Source report(pageTitle)</h1>
      )
    }
  }

class Main extends Component {
  render () {
    return (
    <div>
      <h2>pictures</h2>       
      <CssDemo />
      <StackedLineChart />
      <Doughnut />
      <Table columns={columns} dataSource={data} />
    </div>
    )
  }
}

// Table
const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
];

const App = () => {
  const [theme, setTheme] = useState('light')
 
  return (
    <GeistProvider  themeType={theme}>
      <CssBaseline />
      <Page           
        dotBackdrop={true}
        style={{
          width: "100%",
          height: "100%",
          maxWidth: "1000px",
          paddingTop: "1rem",
        }}>
        <Page.Header>
          <Title />
          <Text h2>introduction...</Text>
        </Page.Header>
        <Home onThemeChange={next => setTheme(next)} />
        <Main />   
        <App1 />
      </Page>
    </GeistProvider>
  )
}

class Demo extends Component {

    // constructor (props) {
    //     super(props);
    //     this.state = {

    //     };
    // }

    render () {
        return (
          <div>
          <App />
          </div>
          )  
    }
  }
ReactDOM.render(<Demo />,document.querySelector('#app'));
